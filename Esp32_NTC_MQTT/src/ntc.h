#ifndef _NTC_H_
#define _NTC_H_

#include "Arduino.h"

#define VCC             3.3             // NodeMCU on board 3.3v vcc
#define R2              10000           // 10k ohm series resistor
#define ADC_resolution  4095            // 10-bit adc
#define A               0.0010757       // thermistor equation parameters
#define B               0.000234125
#define C               0.0000000876741

float NTC_Temp(void);

#endif
