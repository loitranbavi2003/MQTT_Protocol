import paho.mqtt.client as mqtt

mqttBroker = "broker.hivemq.com"
mqttTopic = "MQTT_ESP32"

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker")
    client.subscribe(mqttTopic)

def on_message(client, userdata, message):
    payload = str(message.payload.decode("utf-8"))
    print("Received Temp_Water:", payload)

client = mqtt.Client("MQTT_Client")
client.on_connect = on_connect
client.on_message = on_message

client.connect(mqttBroker, 1883, 60)

client.loop_forever()
