#include "ntc.h"
#include <WiFi.h>
#include <PubSubClient.h>

WiFiClient wifiClient;
PubSubClient client(wifiClient);

#define MQTT_SERVER "broker.hivemq.com"
#define MQTT_PORT 1883
#define MQTT_PUB_TOPIC "MQTT_ESP32"
static char result[8]; // Buffer big enough for 7-character float

// const char* ssid = "Wifi Vung Cao";
// const char* password = "66668888";
const char* ssid = "Embedded AIoT Lab(2G)";
const char* password = "embeddedlab";
 
static float Temp           = 0;
static int count_time       = 0;

void Connect_Wifi();
void Connect_To_Broker();

void setup() 
{
  Serial.begin(112500);
  Connect_Wifi();
  client.setServer(MQTT_SERVER, MQTT_PORT);
  Connect_To_Broker();
}

void loop() 
{
  client.loop();
  if (!client.connected()) 
  {
    Connect_To_Broker();
  }

  if(count_time >= 1000)
  {
    Temp = Temp/1000;
    dtostrf(Temp, 6, 2, result); // Leave room for too large numbers!
    client.publish(MQTT_PUB_TOPIC, result);
    Serial.println(result);
    Temp = 0;
    count_time = 0;
  }
  else
  {
    Temp += NTC_Temp();
    count_time++;
  }

  delay(1);
}

void Connect_Wifi() 
{
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) 
  {
    Serial.print(".");
    delay(500);
  }
  
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void Connect_To_Broker() 
{
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32";
    clientId += String(random(0xffff), HEX);

    if (client.connect(clientId.c_str())) 
    {
      Serial.println("connected");
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 2 seconds");
      delay(2000);
    }
  }
}
