#include "ntc.h"

float NTC_Temp(void)
{
  double Vout, Rth, temperature, adc_value; 

  adc_value = analogRead(34);
  Vout = (adc_value * VCC) / ADC_resolution;
  Rth = (VCC * R2/Vout) - R2;
  temperature = (1 / (A + (B * log(Rth)) + (C * pow((log(Rth)),3))));   // Temperature in kelvin
  temperature = temperature - 273.15;  // Temperature in degree celsius

  return temperature;
}
