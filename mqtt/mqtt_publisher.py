import paho.mqtt.client as mqtt
from random import randrange, uniform
import time

mqttBroker = "broker.hivemq.com"
client = mqtt.Client("Temperture Inside")
client.connect(mqttBroker)

while True:
    randNumber = uniform(50.0, 60.0)
    client.publish("TEMPERATURE", randNumber)
    print("Just published " + str(randNumber) + " to Topic TEMPERATURE")
    time.sleep(1)