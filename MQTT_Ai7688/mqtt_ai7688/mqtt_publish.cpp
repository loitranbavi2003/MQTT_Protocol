#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define PUBLISH     "AI7688H"
#define CLIENT_ID   "client_publish"

using namespace std;

int value = 0;
int count_time = 0;
const char* message;
struct mosquitto* mosq;

int main(int argc, char* argv[])
{
    mosquitto_lib_init();                           
    mosq = mosquitto_new(CLIENT_ID, true, NULL);    

    if (mosq)
    {
        mosquitto_connect(mosq, HOST, PORT, 60);    
    }

    while (1)
    {
        char arr[10];
        if(value != 1)
        {
            value = 1;
        }
        else
        {
            value = 0;
        }
        
        sprintf(arr, "%d", value);
        cout << "Connection remains active" << endl;
        cout << "Sending message on: " << count_time++ << endl;
        cout << "Value: " << arr << endl << endl;
        mosquitto_publish(mosq, NULL, PUBLISH, strlen(arr), arr, 0, false);
        sleep(2); 
        mosquitto_loop(mosq, -1, 1);
    }
}


