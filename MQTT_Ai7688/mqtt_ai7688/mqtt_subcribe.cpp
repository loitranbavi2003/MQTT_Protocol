#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define SUBSCRIBE   "AI7688H"
#define CLIENT_ID   "client_subscribe"

using namespace std;

struct mosquitto *mosq;

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    cout << "Got message from " << message->topic << " topic" << endl;
    cout << "Content: " << (char*) message->payload << endl << endl;
}

int main(int argc, char * argv[])
{
    mosquitto_lib_init();                           // Hàm này khởi tạo thư viện Mosquitto
    mosq = mosquitto_new(CLIENT_ID, true, NULL);    // Hàm này tạo một đối tượng Mosquitto mới

    if (mosq)
    {
        mosquitto_connect(mosq, HOST, PORT, 60);
        mosquitto_message_callback_set(mosq, message_callback);
        mosquitto_subscribe(mosq, NULL, SUBSCRIBE, 0);
    }

    while (1)
    {
        mosquitto_loop(mosq, 3000, 1); // Xử lý các sự kiện liên quan đến kết nối MQTT trong một khoảng thời gian cụ thể 3s
    }

    mosquitto_destroy(mosq); // Hủy đối tượng Mosquitto
    mosquitto_lib_cleanup(); // Dọn dẹp và giải phóng tài nguyên liên quan đến thư viện Mosquitto
    return 0;
}

/*
    mosquitto_subscribe():

-mosq: Con trỏ tới đối tượng Mosquitto, đại diện cho kết nối MQTT.
-NULL: Tham số này đại diện cho con trỏ tới một số thông điệp (message) MQTT đặc biệt được sử dụng cho đăng ký chủ đề. 
    Trong trường hợp này, giá trị NULL được truyền, tức là không có thông điệp đặc biệt.
-SUBSCRIBE: Chuỗi ký tự chứa tên chủ đề MQTT mà chương trình muốn đăng ký để nhận thông điệp.
-0: Mức độ ưu tiên của chủ đề đăng ký. Trong trường hợp này, giá trị 0 được truyền,
    tức là không có mức độ ưu tiên đặc biệt cho chủ đề.
*/
