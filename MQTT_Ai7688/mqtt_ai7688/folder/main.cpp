#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "led.h"

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define SUBSCRIBE   "AI7688H"
#define CLIENT_ID   "client_subscribe"

using namespace std;

struct mosquitto *mosq;

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);


int main(int argc, char **argv)
{
	int ret = -1;

    mosquitto_lib_init();
    mosq = mosquitto_new(CLIENT_ID, true, NULL);
    if (mosq)
    {
        mosquitto_connect(mosq, HOST, PORT, 60);
        mosquitto_message_callback_set(mosq, message_callback);
        mosquitto_subscribe(mosq, NULL, SUBSCRIBE, 0);
    }

	if (gpio_mmap())
	{
		return -1;
	}
	
	printf("set pin 11 output 0\n");
	mt76x8_gpio_set_pin_direction(GPIO_LED, 1);
	mt76x8_gpio_set_pin_value(GPIO_LED, 0);

	while (1)
	{
        mosquitto_loop(mosq, 3000, 1);
	}
	close(gpio_mmap_fd);

	return ret;
}

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	int value = 0;
	char *arr;
	printf("got message '%d. %s' for topic '%s'\n", message->payloadlen, (char*) message->payload, message->topic);
	arr = (char*) message->payload;
	cout << "Content: " <<  arr[0]-'0' << endl;
	mt76x8_gpio_set_pin_value(GPIO_LED, (arr[0]-'0'));
}

