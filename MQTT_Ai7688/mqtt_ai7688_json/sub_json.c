#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <json-c/json.h>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define SUBSCRIBE   "AI7688H"
#define CLIENT_ID   "client_subscribe"

struct mosquitto *mosq;
char message_topic_str[256];
char message_payload_str[1024];

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);

int main(int argc, char *argv[]) 
{
    mosquitto_lib_init();
    mosq = mosquitto_new(CLIENT_ID, true, NULL);

    if (mosq) 
    {
        mosquitto_connect(mosq, HOST, PORT, 60);
        mosquitto_message_callback_set(mosq, message_callback);
        mosquitto_subscribe(mosq, NULL, SUBSCRIBE, 0);
    }

    while (1) 
    {
        mosquitto_loop(mosq, 3000, 1);
    }

    return 0;
}

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message) 
{
    printf("Got message from %s topic\n", message->topic);
    printf("Content: %s\n", (char *)message->payload);
    strcpy(message_payload_str, (char *)message->payload);
    strcpy(message_topic_str, message->topic);

    json_object *root = json_tokener_parse(message_payload_str);
    if (root != NULL) 
    {
        json_object *value_led = json_object_object_get(root, "value_led");

        if (value_led != NULL) 
        {
            int led_state = json_object_get_int(value_led);
            printf("Value_led: %d\n", led_state);
        }

        json_object_put(root);
    }
    
    printf("\n");
}
