#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <json-c/json.h>  

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define SUBSCRIBE   "AI7688H"
#define CLIENT_ID   "client_subscribe"

using namespace std;

struct mosquitto *mosq;
string message_topic_str;
string message_payload_str;

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);


int main(int argc, char * argv[])
{
    mosquitto_lib_init();
    mosq = mosquitto_new(CLIENT_ID, true, NULL);

    if (mosq)
    {
        mosquitto_connect(mosq, HOST, PORT, 60);
        mosquitto_message_callback_set(mosq, message_callback);
        mosquitto_subscribe(mosq, NULL, SUBSCRIBE, 0);
    }

    while (1)
    {
        mosquitto_loop(mosq, 3000, 1);
    }

    return 0;
}

void message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
    cout << "Got message from " << message->topic << " topic" << endl;
    cout << "Content: " << (char*) message->payload;
    message_payload_str = (char*) message->payload;
    message_topic_str = message->topic;

    json_object *root = json_tokener_parse(message_payload_str.c_str()); // Parse JSON using JSON-C
    if (root != nullptr)
    {
        json_object *address_obj = json_object_object_get(root, "address");
        json_object *value_1_obj = json_object_object_get(root, "value_1");
        json_object *value_2_obj = json_object_object_get(root, "value_2");

        if (address_obj != nullptr && value_1_obj != nullptr && value_2_obj != nullptr)
        {
            cout << "Address: " << json_object_get_int(address_obj) << endl;
            cout << "Value_1: " << json_object_get_int(value_1_obj) << endl;
            cout << "Value_2: " << json_object_get_int(value_2_obj) << endl;
        }

        json_object_put(root); // Release JSON object
    }
    cout << endl;
}
