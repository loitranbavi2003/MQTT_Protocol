#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <json-c/json.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define PUBLISH     "AI7688H"
#define CLIENT_ID   "client_publish"

int count_time = 0, flag = 0;
const char* msg;
struct mosquitto* mosq;

int main(int argc, char* argv[]) {
    mosquitto_lib_init();
    mosq = mosquitto_new(CLIENT_ID, true, NULL);

    if (mosq) {
        mosquitto_connect(mosq, HOST, PORT, 60);
    }

    while (1) 
    {
        json_object* message = json_object_new_object();
        if(flag == 0)
        {
            flag = 1;
            json_object_object_add(message, "value_led", json_object_new_int(1));
        }
        else
        {
            flag = 0;
            json_object_object_add(message, "value_led", json_object_new_int(0));
        }

        const char* json_msg_str = json_object_to_json_string(message);
        msg = json_msg_str;

        printf("Connection remains active\n");
        printf("Sending message on: %d\n", count_time++);
        printf("Value: %s\n\n", msg);
        mosquitto_publish(mosq, NULL, PUBLISH, strlen(msg), msg, 0, false);

        sleep(2);
        mosquitto_loop(mosq, -1, 1);
    }

    mosquitto_lib_cleanup();
    return 0;
}
