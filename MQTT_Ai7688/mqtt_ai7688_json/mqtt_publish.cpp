#include <mosquitto.h>
#include <mqtt_protocol.h>
#include <json-c/json.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#define HOST        "Mqtt.mysignage.vn"
#define PORT        1883
#define PUBLISH     "AI7688H"
#define CLIENT_ID   "client_publish"

using namespace std;

int count_time = 0;
const char* msg;
struct mosquitto* mosq;

int main(int argc, char* argv[])
{
    mosquitto_lib_init();
    mosq = mosquitto_new(CLIENT_ID, true, NULL);

    if (mosq)
    {
        mosquitto_connect(mosq, HOST, PORT, 60);
    }

    json_object* message = json_object_new_object();
    json_object_object_add(message, "address", json_object_new_int(0x03));
    json_object_object_add(message, "value_1", json_object_new_int(6));
    json_object_object_add(message, "value_2", json_object_new_int(8));

    const char* json_msg_str = json_object_to_json_string(message);
    msg = json_msg_str;

    while (1)
    {
        cout << "Connection remains active" << endl;
        cout << "Sending message on: " << count_time++ << endl;
        cout << "Value: " << msg << endl << endl;
        mosquitto_publish(mosq, NULL, PUBLISH, strlen(msg), msg, 0, false);
        
        sleep(1); 
        mosquitto_loop(mosq, -1, 1);
    }

    mosquitto_lib_cleanup();
    return 0;
}