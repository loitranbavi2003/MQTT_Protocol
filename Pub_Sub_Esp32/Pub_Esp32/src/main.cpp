#include <WiFi.h>
#include <PubSubClient.h>


const char* ssid = "LAB_2G";
const char* password = "embeddedlab";

char result[8]; // Buffer big enough for 7-character float

#define MQTT_SERVER "broker.hivemq.com"
#define MQTT_PORT 1883
#define MQTT_PUB_TOPIC "MQTT_ESP32/PUB"
#define MQTT_SUB_TOPIC "MQTT_ESP32/SUB"
 
WiFiClient wifiClient;
PubSubClient client(wifiClient);
 
void Connect_Wifi();
void Connect_MQTTBroker();
 
int count_time = 1000;
float Temp = 30.25, Hum = 50.5;
 
void setup() 
{
  Serial.begin(9600);
  Serial.setTimeout(500);

  Connect_Wifi();
  client.setServer(MQTT_SERVER, MQTT_PORT);
  Connect_MQTTBroker();

  Serial.println("----------------- Start Transfer -----------------");
}

void loop() 
{
  client.loop();
  if (!client.connected()) 
  {
    Connect_MQTTBroker();
  }
  
  if(Temp < 36)
  {
    dtostrf(Temp, 6, 2, result); // Leave room for too large numbers!
    client.publish(MQTT_PUB_TOPIC, result);
    Temp++;
  }
  else
  {
    Temp = 30.25;
  }
  while(count_time--)
  {
    if(count_time == 1)
    {
      count_time = 1000;
      break;
    }
    delay(1);
  } 

  if(Hum < 56)
  {
    dtostrf(Hum, 6, 2, result); // Leave room for too large numbers!
    client.publish(MQTT_PUB_TOPIC, result);
    Hum++;
  }
  else
  {
    Hum = 50.5;
  }
  while(count_time--)
  {
    if(count_time == 1)
    {
      count_time = 1000;
      break;
    }
    delay(1);
  } 
}

void Connect_MQTTBroker() 
{
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32";
    clientId += String(random(0xffff), HEX);

    if (client.connect(clientId.c_str())) 
    {
      Serial.println("connected");
      // client.subscribe(MQTT_PUB_TOPIC);
      // client.subscribe(MQTT_SUB_TOPIC);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 2 seconds");
      delay(1000);
    }
  }
}

void Connect_Wifi() 
{
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    Serial.println(".");
    delay(500);
  }
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();
}
