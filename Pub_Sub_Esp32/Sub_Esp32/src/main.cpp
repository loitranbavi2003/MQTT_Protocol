#include <WiFi.h>
#include <PubSubClient.h>


String data;

const char* ssid = "LAB_2G";
const char* password = "embeddedlab";
 
#define MQTT_SERVER "broker.hivemq.com"
#define MQTT_PORT 1883
#define MQTT_PUB_TOPIC "MQTT_ESP32/PUB"
#define MQTT_SUB_TOPIC "MQTT_ESP32/SUB"
 
WiFiClient wifiClient;
PubSubClient client(wifiClient);
 
void Connect_Wifi();
void Connect_MQTTBroker();
void CallBack(char* topic, byte *payload, unsigned int length);
 
float Temp = 0, Hum = 0, Convert_T_F = 0;
 
void setup() 
{
  Serial.begin(9600);
  Serial.setTimeout(500);

  Connect_Wifi();
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(CallBack);
  Connect_MQTTBroker();

  Serial.println("----------------- Start Transfer -----------------");
}

void loop() 
{
  client.loop();
  if (!client.connected()) 
  {
    Connect_MQTTBroker();
  }
}

void CallBack(char* topic, byte *payload, unsigned int length) 
{
  char status[20];
  Serial.println("-------new message from broker-----");
  Serial.print("topic: ");
  Serial.println(topic);
  Serial.print("message: ");
  Serial.write(payload, length);
  Serial.println();
  for(int i = 0; i < length; i++)
  {
    data += (char)payload[i];
  }

  if(String(topic) == MQTT_PUB_TOPIC)
  {
    //Serial.println(data);
    Convert_T_F = data.toFloat();
    if(Convert_T_F < 37)
    {
      Temp = Convert_T_F;
      Serial.print("Temp : ");
      Serial.println(Temp);
    }
    else
    {
      Hum = Convert_T_F;
      Serial.print("Hum : ");
      Serial.println(Hum);
    }
  }
  data.remove(0,data.length());
}

void Connect_MQTTBroker() 
{
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32";
    clientId += String(random(0xffff), HEX);

    if (client.connect(clientId.c_str())) 
    {
      Serial.println("connected");
      client.subscribe(MQTT_PUB_TOPIC);
      // client.subscribe(MQTT_SUB_TOPIC);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 2 seconds");
      delay(1000);
    }
  }
}

void Connect_Wifi() 
{
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    Serial.println(".");
    delay(500);
  }
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  Serial.println();
}
