import json

data = {
    "user": [
        {
            "name": "Loi Tran",
            "age": 19,
            "weight": 56
        },
        {
            "name": "Loi Tran",
            "age": 19,
            "weight": 60
        }
    ]
}

json_str = json.dumps(data, indent = 4)
with open("data_file.json", "w") as write_file:
    json.dump(data, write_file, indent = 4)
    # write_file.write(json_str)

print(json_str)

# dump() ghi dữ liệu JSON vào 1 file cụ thể
# dumps() trả về chuỗi JSON