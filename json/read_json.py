import json

with open('data_file.json', 'r') as read_file:
    json_object = json.loads(read_file.read())

data = json.dumps(json_object, indent=4)
print(data)