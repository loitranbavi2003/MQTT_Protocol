#include <WiFi.h>
#include <PubSubClient.h>

#define LED1 17
#define LED2 5

String data;

const char* ssid = "Wifi Vung Cao";
const char* password = "66668888";
 
#define MQTT_SERVER "broker.hivemq.com"
#define MQTT_PORT 1883
#define MQTT_LED1_TOPIC "MQTT_ESP32/LED1"
#define MQTT_LED2_TOPIC "MQTT_ESP32/LED2"
 
WiFiClient wifiClient;
PubSubClient client(wifiClient);
 
void setup_wifi();
void connect_to_broker();
void callback(char* topic, byte *payload, unsigned int length);
 
 
void setup() 
{
  Serial.begin(9600);
  Serial.setTimeout(500);
  setup_wifi();
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);
  connect_to_broker();
  Serial.println("Start transfer");

  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
}

void loop() 
{
  client.loop();
  if (!client.connected()) 
  {
    connect_to_broker();
  }
}

void callback(char* topic, byte *payload, unsigned int length) 
{
  char status[20];
  Serial.println("-------new message from broker-----");
  Serial.print("topic: ");
  Serial.println(topic);
  Serial.print("message: ");
  Serial.write(payload, length);
  Serial.println();
  for(int i = 0; i < length; i++)
  {
    data += (char)payload[i];
  }
  Serial.println(data);
  if(String(topic) == MQTT_LED1_TOPIC)
  {
    if(String(data) == "OFF")
    {
      digitalWrite(LED1, LOW);
      Serial.println("LED1 OFF");
    }
    else if(String(data) == "ON")
    {
      digitalWrite(LED1, HIGH);
      Serial.println("LED1 ON");
    }
  }
 
  if(String(topic) == MQTT_LED2_TOPIC)
  {
    if(String(data) == "OFF")
    {
      digitalWrite(LED2, LOW);
      Serial.println("LED2 OFF");
    }
    else if(String(data) == "ON")
    {
      digitalWrite(LED2, HIGH);
      Serial.println("LED2 ON");
    }
  }
  data.remove(0,data.length());
}

void setup_wifi() 
{
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void connect_to_broker() 
{
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) 
    {
      Serial.println("connected");
      client.subscribe(MQTT_LED1_TOPIC);
      client.subscribe(MQTT_LED2_TOPIC);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 2 seconds");
      delay(2000);
    }
  }
}
