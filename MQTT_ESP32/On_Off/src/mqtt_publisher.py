import paho.mqtt.client as mqtt
import time

mqttBroker = "broker.hivemq.com"
client = mqtt.Client("led")
client.connect(mqttBroker)

while True:
    led_on = "ON"
    led_off = "OFF"
    client.publish("MQTT_ESP32/LED1", led_on)
    print("Status Led: ",led_on)
    time.sleep(1)
    client.publish("MQTT_ESP32/LED1", led_off)
    print("Status Led: ",led_off)
    time.sleep(1)