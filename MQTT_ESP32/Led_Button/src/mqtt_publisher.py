import paho.mqtt.client as mqtt
import time

mqttBroker = "broker.hivemq.com"
client = mqtt.Client("led")
client.connect(mqttBroker)

while True:
    status_led = 1
    client.publish("ESP32Client", status_led)
    print("Status Led: ",status_led)
    time.sleep(1)