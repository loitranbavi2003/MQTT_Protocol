#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>

#define LED1 17
#define LED2 5

WiFiClient wifiClient;
PubSubClient client(wifiClient);

const char* ssid = "Wifi Vung Cao";
const char* password = "66668888";

#define MQTT_SERVER "broker.hivemq.com"
#define MQTT_PORT 1883
#define MQTT_LED1_TOPIC "MQTT_ESP32/LED1"
#define MQTT_LED2_TOPIC "MQTT_ESP32/LED2"

void setup_wifi();
void connect_to_broker();
void callback(char* topic, byte *payload, unsigned int length);
void publish_led_status(const char* topic, const char* status);
void PWM_Led(const char* topic, int brightness);

void setup() 
{
  Serial.begin(9600);
  setup_wifi();
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callback);
  connect_to_broker();

  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
}

void loop() 
{
  client.loop();
  if (!client.connected()) {
    connect_to_broker();
  }
}

void callback(char* topic, byte* payload, unsigned int length) 
{
  Serial.println("-------new message from broker-----");
  Serial.print("topic: ");
  Serial.println(topic);
  Serial.print("message: ");
  Serial.write(payload, length);
  Serial.println();

  // Deserialize the JSON message
  StaticJsonDocument<256> doc;
  DeserializationError error = deserializeJson(doc, payload, length);
  if (error) 
  {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }

  // Get the LED status from the JSON message
  const char* topic_publish = doc["LED"]["TOPIC"];
  const char* status_led    = doc["LED"]["STATUS"];
  int         brightness    = doc["LED"]["BRIGHTNESS"];

  // Publish the LED status to MQTT
  //publish_led_status(topic_publish, status_led);
  PWM_Led(topic_publish, brightness);
}

void PWM_Led(const char* topic, int brightness)
{
  if (strcmp(topic, MQTT_LED1_TOPIC) == 0) 
  {
    analogWrite(LED1, brightness);
  } 
  else if (strcmp(topic, MQTT_LED2_TOPIC) == 0) 
  {
    analogWrite(LED2, brightness);
  } 
}

void publish_led_status(const char* topic, const char* status) 
{
  int led_pin;
  if (strcmp(topic, MQTT_LED1_TOPIC) == 0) 
  {
    led_pin = LED1;
  } 
  else if (strcmp(topic, MQTT_LED2_TOPIC) == 0) 
  {
    led_pin = LED2;
  } 
  else 
  {
    Serial.println("Invalid topic");
    return;
  }

  if (strcmp(status, "ON") == 0) 
  {
    digitalWrite(led_pin, HIGH);
    Serial.print(topic);
    Serial.println(": ON");
    client.publish(topic, "ON");
  } 
  else if (strcmp(status, "OFF") == 0) 
  {
    digitalWrite(led_pin, LOW);
    Serial.print(topic);
    Serial.println(": OFF");
    client.publish(topic, "OFF");
  } 
  else 
  {
    Serial.println("Invalid LED status");
  }
}

void setup_wifi() 
{
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}
 
void connect_to_broker() 
{
  while (!client.connected()) 
  {
    Serial.print("Attempting MQTT connection...");
    String clientId = "ESP32";
    clientId += String(random(0xffff), HEX);
    if (client.connect(clientId.c_str())) 
    {
      Serial.println("connected");
      client.subscribe(MQTT_LED1_TOPIC);
      client.subscribe(MQTT_LED2_TOPIC);
    } 
    else 
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 2 seconds");
      delay(2000);
    }
  }
}
