import paho.mqtt.client as mqtt
import time
import json

brightness_1 = 0
brightness_2 = 255

Led_1 = {
    "LED": {
        "TOPIC": "MQTT_ESP32/LED1",
        "STATUS": "ON",
        "BRIGHTNESS": brightness_1
    }
}

Led_2 = {
    "LED": {
        "TOPIC": "MQTT_ESP32/LED2",
        "STATUS": "ON",
        "BRIGHTNESS": brightness_2
    }
}

mqttBroker = "broker.hivemq.com"
client = mqtt.Client("led")
client.connect(mqttBroker)

while True:
    json_led_1 = json.dumps(Led_1)
    json_led_2 = json.dumps(Led_2)

    client.publish("MQTT_ESP32/LED1", json_led_1)
    client.publish("MQTT_ESP32/LED2", json_led_2)

    print(json_led_1)
    print(json_led_2)

    if brightness_1 < 245:
        brightness_1 += 15
    else:
        brightness_1 = 0
    Led_1["LED"]["BRIGHTNESS"] = brightness_1

    if brightness_2 > 0:
        brightness_2 -= 15
    else:
        brightness_2 = 255
    Led_2["LED"]["BRIGHTNESS"] = brightness_2

    time.sleep(1)
