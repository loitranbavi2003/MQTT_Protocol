import paho.mqtt.client as mqtt
import time
import json

brightness = 0

data = {
    "LED": {
        "TOPIC": "MQTT_ESP32/LED1",
        "STATUS": "ON",
        "BRIGHTNESS": brightness
    }
}

mqttBroker = "broker.hivemq.com"
client = mqtt.Client("led")
client.connect(mqttBroker)

while True:
    json_data = json.dumps(data, indent=4)
    client.publish("MQTT_ESP32/LED1", json_data)
    print(json_data)
    if brightness < 245:
        brightness += 15
    else:
        brightness = 0
    data["LED"]["BRIGHTNESS"] = brightness
    print(brightness)
    time.sleep(1.5)
